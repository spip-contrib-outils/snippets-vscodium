# Snippets SPIP pour vscodium

https://code.visualstudio.com/docs/editor/userdefinedsnippets

## installation : 
- cloner ce repo commee dossier des snippets utilisateurs de VSCodium:

Sous windows : ```C:\Users\mon_utilisateur\AppData\Roaming\VSCodium\User\snippets```

Sous MacOS : ```$HOME/Library/Application Support/VSCodium/User/snippets```

Sous Linux : ```$HOME/.config/VSCodium/User/snippets```


## NB: UltiSnips :
le dossier UltiSnips intégré dans ce repo permet aux utilisateurs de Vim d'utiliser les mêmes snippets (après conversion)

cf https://github.com/sirver/UltiSnips 

merci de ne PAS le supprimer
